package com.sda.springbootdemo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring.datasource")
public class DBConfiguration {

	private String driverClassName;
	private String url;
	private String userName;
	private String password;
	
	
	public String getDriverClassName() {
		return driverClassName;
	}
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Profile("autumn")
	@Bean
	public String devDBConnection() {
		
		String output = "Autumn";
		System.out.println(output);
		return output;
	}
	
	@Profile("spring")
	@Bean
	public String devDBConnectionOne() {
		
		String output = "Spring";
		System.out.println(output);
		return output;
	}
	
	@Profile("winter")
	@Bean
	public String devDBConnectionTwo() {
		
		String output = "Winter";
		System.out.println(output);
		return output;
	}
	
	@Profile("summer")
	@Bean
	public String devDBConnectionThree() {
		
		String output = "Summer";
		System.out.println(output);
		return output;
	}
}

