package com.sda.springbootdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.sda.springbootdemo.service.Season;



@SpringBootApplication
public class SpringBootDemoApplication implements CommandLineRunner {
	
	@Autowired
	Season season;


	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemoApplication.class, args);
		

	}
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Report: " + season.report());
	

	
	}
	
}
