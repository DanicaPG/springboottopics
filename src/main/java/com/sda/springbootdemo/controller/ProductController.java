package com.sda.springbootdemo.controller;

import com.sda.springbootdemo.model.*;
import com.sda.springbootdemo.service.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@GetMapping("products/all")
	public List<Product> retrieveAllProducts(){
		return productService.retriveAllProducts();
		
	}
	@GetMapping("products/{id}")
	public Product  retrieveProductById(@PathVariable String id){
		return productService.retrieveProductById(id);
	}
	
	@PostMapping("products")
	public Product addProduct(@RequestBody Product product) {
		return productService.addNewProduct(product);
	}
	
	@PutMapping("products/{id}")
	public Product  updateProductById(@PathVariable String id, @RequestBody Product updateProduct){
		return productService.updateProductPrice(id, updateProduct.getName(), updateProduct.getPrice());
	}
	
	@DeleteMapping("products/{id}")
	public ResponseEntity<Product>  deleteProductById(@PathVariable String id){
		  if (productService.retrieveProductById(id)== null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else
		{
			productService.deleteProductById(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}
}
	
	
