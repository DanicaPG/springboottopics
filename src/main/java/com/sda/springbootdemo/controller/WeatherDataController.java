package com.sda.springbootdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sda.springbootdemo.service.WeatherDataService;

@RestController
@RequestMapping("accuweather")
public class WeatherDataController {
	
	@Autowired
	WeatherDataService weatherDataService;
	
	 
	 @GetMapping("url/all")
	 public String getAvailableDailyForecastInformationURL()
	 {
	   return weatherDataService.getDailyForecastDay1URL();
	 }
	 

}
