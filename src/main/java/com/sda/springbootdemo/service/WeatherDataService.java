package com.sda.springbootdemo.service;

import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("url")
public class WeatherDataService {
	
	@Value("${accuweather.dailyforecast.day1.url}")
	private String dailyForecastDay1URL;
	
	@Value("${accuweather.dailyforecast.days10.url}")
	private String dailyForecastDay10URL;

	@Value("${accuweather.dailyforecast.days15.url}")
	private String dailyForecastDay15URL;
	
	@Value("${accuweather.dailyforecast.days5.url}")
	private String dailyForecastDay5URL;

	@GetMapping("/day1url")
	public String getDailyForecastDay1URL() {
		return dailyForecastDay1URL;
	}

	public void setDailyForecastDay1URL(String dailyForecastDay1URL) {
		this.dailyForecastDay1URL = dailyForecastDay1URL;
	}
	@GetMapping("/days10url")
	public String getDailyForecastDay10URL() {
		return dailyForecastDay10URL;
	}

	public void setDailyForecastDay10URL(String dailyForecastDay10URL) {
		this.dailyForecastDay10URL = dailyForecastDay10URL;
	}

	public String getDailyForecastDay15URL() {
		return dailyForecastDay15URL;
	}

	public void setDailyForecastDay15URL(String dailyForecastDay15URL) {
		this.dailyForecastDay15URL = dailyForecastDay15URL;
	}

	public String getDailyForecastDay5URL() {
		return dailyForecastDay5URL;
	}

	public void setDailyForecastDay5URL(String dailyForecastDay5URL) {
		this.dailyForecastDay5URL = dailyForecastDay5URL;
	}
	
	
}
