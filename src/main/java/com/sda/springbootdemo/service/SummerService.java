package com.sda.springbootdemo.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile({"summer"})
public class SummerService implements Season{

	@Override
	public String report() {
		String output = "Summer";
		System.out.println(output);
		return output;
	}

}
