package com.sda.springbootdemo.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile({"winter"})
public class WinterService implements Season{

	@Override
	public String report() {
		String output = "Winter";
		System.out.println(output);
		return output;
	}
	

}
