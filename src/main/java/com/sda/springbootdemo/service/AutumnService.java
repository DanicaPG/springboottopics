package com.sda.springbootdemo.service;


import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Service
@Profile({"autumn", "default"})
public class AutumnService implements Season {

	@Override
	public String report() {
		String output = "Autumn";
		System.out.println(output);
		return output;
	}
	}

	
		

	
	

