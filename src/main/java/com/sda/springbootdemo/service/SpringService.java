package com.sda.springbootdemo.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile({"spring"})
public class SpringService implements Season{

	@Override
	public String report() {
		String output = "Spring";
		System.out.println(output);
		return output;
	}

}
