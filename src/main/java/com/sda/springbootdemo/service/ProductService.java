package com.sda.springbootdemo.service;

import com.sda.springbootdemo.model.*;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Scanner;

import org.springframework.stereotype.Service;

@Service
public class ProductService {
	
	private static Map<String, Product> productMap = new HashMap<> ();
	//initial data
	
	static {
		
		Product coffee1 = new Product();
		coffee1.setId("1");
		coffee1.setName("Latte");
		coffee1.setPrice(150.0);
		productMap.put(coffee1.getId(),coffee1);
		
		Product coffee2 = new Product();
		coffee2.setId("2");
		coffee2.setName("Cappucino");
		coffee2.setPrice(155.0);
		productMap.put(coffee2.getId(),coffee2);
		
		Product coffee3 = new Product();
		coffee3.setId("3");
		coffee3.setName("American0");
		coffee3.setPrice(151.0);
		productMap.put(coffee3.getId(),coffee3);
		
		
	}
	
	// get all product
	
	public List <Product> retriveAllProducts(){
		return productMap.values().stream().collect(Collectors.toList());
			
	}
	
	// get product by id
	
	public Product retrieveProductById(String id) {
		return productMap.get(id);
	}
	
	private  SecureRandom secureRandom = new SecureRandom();
	Scanner scan = new Scanner(System.in);
	// add product 
	
	public Product addNewProduct(Product newProduct) {
		
		Product product = new Product();
		product.setId(String.valueOf(Math.abs(secureRandom.nextInt())));
		product.setName(String.valueOf(scan.nextLine()));
		product.setPrice(Double.valueOf(scan.nextDouble()));
		
		return product;
	}
	
	// update product by id
	
	public Product updateProductPrice(String id, String newName, double newPrice) {
		Product product = null;
		if(productMap.containsKey(id)) {
			product = productMap.get(id);
			product.setPrice(newPrice);
			product.setName(newName);
			
		}
		
		return product;
		
	}
	// delete product by id
	public void deleteProductById(String id) {
		if(productMap.containsKey(id)) {
			productMap.remove(id);
			
		}
		
	}
	
	
	
	

}
