package com.sda.springbootdemo;

import com.sda.springbootdemo.service.Season;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("spring")
class SpringBootDemoApplicationTests {

	@Autowired
	Season season;
	
	
	
	@Test
	void testWinterService() {
		String chars = "Spring";
		Assertions.assertTrue(season.report().contains(chars));
	}


}







